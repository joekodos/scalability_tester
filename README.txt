Welcome to the scalability_tester repo!

scale_controller.py     -       Main controller script
scrap_code.py           -       Place for temp data
bll/                    -       business logic layer
bll/db.py               -       define API calls
bll/dal/                -       data access layer
bll/dal/apiv2.py        -       define requests for PureCloud Public API v2
bll/dal/privateAPI.py   -       define requests for PureCloud Private API
bll/dal/excel.py        -       interface with excel
bll/dal/config.py       -       stores config data

More details coming soon!

For now, create a call definition in db.py, and make an entry for it in either apiv2.py or privateAPI.py
Then, do stuff with it in the scale_controller.py