import platform
import dal


def get_oauth2_access_token(env, authorization):
    """Login to Public API V2"""
    # Get a Client Authorization Grant
    url = "https://login.inin" + env + ".com/oauth/token"

    # Method Headers:
    requestHeaders = {
        'Authorization': 'Basic ' + authorization,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    # Method body:
    requestBody = {
        'grant_type': 'client_credentials'
    }
    # Send the request with parameters and store it in login_response
    response = dal.apiv2.get_oauth2_access_token(url, requestBody, requestHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    response_json = response.json()
    print "response_json response_json response_json response_json"
    print response_json

    # Store the token
    access_token = response_json["access_token"]
    return access_token



def private_publish_flow(userId, orgId, flowId, version):

    """Publish a Flow using Private API"""
    #orgId = "8a3a828a-5e49-4cc5-a59b-627a1e36f2f6"
    #flowId = "1bc85720-d6a1-9842-b0bb-385b317ad547"
    #version = "1.0"
    host = "automate.us-east-1.inindca.com"

    # This is the process automation URL used to publish a flow
    publish_flow_url = "https://" + host + "/processautomation/v1/organizations/" + orgId + "/flows/definitions/" + flowId + "/commands/architectPublish?version=" + version

    # Method Headers
    publish_flow_headers = {'Content-Type': 'application/json',
                               'ININ-User-Id': '%s' % userId}

    # Specify request body json data and headers
    publish_flow_response = dal.privateAPI.publish_flow(publish_flow_url, publish_flow_headers)


    """Store the publish_flow_response """

    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    publish_flow_response = publish_flow_response.json()
    return publish_flow_response



def launch_flow(host, flowConfigId, launchType, access_token):

    """Launch a flow using Public API v2"""
    # This is the process automation URL used to publish a flow
    launch_flow_url = "https://" + host + "/api/v2/processautomation/flows/instances"

    # Method Headers
    launch_flow_headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer %s' % access_token
    }
    # Method Body
    launch_flow_body = {
        "flowConfigId": '%s' % flowConfigId,
        "launchType": '%s' % launchType
    }
    # Specify request body json data and headers
    launch_flow_response = dal.apiv2.launch_flow(launch_flow_url, launch_flow_body, launch_flow_headers)

    """Store the launch_flow_response """
    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    launch_flow_response = launch_flow_response.json()
    return launch_flow_response
















