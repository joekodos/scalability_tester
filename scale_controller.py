#!/usr/bin/python

# ----------------------------------------------------
# Written by Joseph Kodos
# 08/15/2016
# A Scalability Testing Controller
# for the PureCloud Process Automation Service
#
# scale_controller.py
#
# ----------------------------------------------------
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
from openpyxl import load_workbook
import bll
import sys
import os
import base64
import sys

# Title
print "\n"
print "                                             |---- SCALE TEST CONTROLLER v1.0 ----|"
print "                                      |---- for PureCloud Process Automation Service  ----|"
print "\n"
"""Login to Public API V2"""
print "   Logging into Public API V2...  "
print "\n"

env = "dca"
clientId = "eb9fa343-0467-4fdc-9f9d-a928eb94c55b"
clientSecret = "Wz-bhaqXdWr3fVS_wgoIX46IiV-zvcJkanVzAC6vOU8"
authorization = base64.b64encode(clientId + ':' + clientSecret)
access_token = bll.db.get_oauth2_access_token(env, authorization)

print "  -Access Token Acquisition Successful-"
print "  -Access Token is: " + access_token
print "---------------------------------"

host = "api.inin" + env + ".com"
orgId = "8a3a828a-5e49-4cc5-a59b-627a1e36f2f6"

print "Config is:"
print "Host: " + host
print "Org: " + orgId
print "---------------------------------"

print " [1] Test #1: Publish Flow (privateAPI)"
print " [2] Test #2: Launch Flow (publicAPI)"
print " [3] Test #3: null"
whatDo = raw_input(" What do you want to do? [1|2|3]: ")


'''Do Things Based on Selected Test'''

if whatDo == "1":
    # execute test #1
    print "Test #1 selected!"
    print "Upload a flow definition to S3 and provide version number here to publish"

    userId = "f44e8e0b-826a-456a-9d3c-384969b84ad6"
    flowConfigId = "1bc85720-d6a1-9842-b0bb-385b317ad547"

    print "Automatically entering data..."
    print "Setting userId = " + userId
    print "Setting orgId = " + orgId
    print "Setting flowConfigId = " + flowConfigId

    flowVersion = raw_input(" What version are you publishing to? [1.0|2.0|etc...]: ")
    private_publish_flow_response = bll.db.private_publish_flow(userId, orgId, flowConfigId, flowVersion)
    print "----private_publish_flow_response-----: "
    print private_publish_flow_response

elif whatDo == "2":
    # execute test #2
    print "Test #2 selected!"
    print "Launch a flow!"

    flowConfigId = "1bc85720-d6a1-9842-b0bb-385b317ad547"
    launchType = "NORMAL"

    print "Automatically entering data..."
    print "Setting flowConfigId = " + flowConfigId
    print "Setting launchType = " + launchType

    launch_flow_response = bll.db.launch_flow(host, flowConfigId, launchType, access_token)
    print "----launch_flow_response-----: "
    print launch_flow_response



elif whatDo == "3":
    # execute test #3
    print "Test #3 selected!"


else:
    print "No valid option selected!"






























